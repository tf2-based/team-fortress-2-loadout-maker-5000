import random
import sys
from PyQt5 import uic, QtGui
from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
import clipboard
import numpy as np

def inargs(arg):
    return True if arg in sys.argv else False

class UI(QMainWindow):
    def __init__(self):
        super(UI, self).__init__()
        uic.loadUi("app.ui", self)
        self.show()
        if inargs("-debug"):
            print("debug on")
        # remember, code here
        # functions are OUT of __init__
        self.setWindowIcon(QtGui.QIcon("icon.ico"))
        self.pushButton.clicked.connect(self.genld)
        self.pushButton_2.clicked.connect(self.copy)
    def genld(self):
        # podemb please redo this to use the "loadouts all.txt" file and not this fuckin mess bro its UNIMAGINABLY HORRID
        if (game := self.game_cbox.currentText()) == "Vanilla":
            if (merc := self.merc_cbox.currentText()) == "Scout":
                pri = ["Scattergun", "Force-a-Nature", "Shortstop", "Soda Popper", "Baby Face\'s Blaster", "Back Scatter"]
                sec = ["Pistol", "Lugermorph", "Bonk! Atomic Punch", "Crit-a-Cola", "Mad Milk", "Mutated Milk", "Winger", "Pretty Boy\'s Pocket Pistol", "Flying Guillotine", "C.A.P.P.E.R"]
                mle = ["Bat", "Frying Pan", "Conscientious Objector", "Freedom Staff", "Bat Outta Hell", "Memory Maker", "Ham Shank", "Necro Smasher", "Crossing Guard", "Prinny Machete", "Holy Mackerel", "Unarmed Combat", "Batsaber", "Saxxy", "Golden Frying Pan", "Sandman", "Candy Cane", "Boston Basher", "Three-Rune Blade", "Sun-on-a-Stick", "Fan O\'War", "Atomizer", "Wrap Assassin"]
                pda = ["(not applicable)"]
            elif merc == "Soldier":
                pri = ['Black Box', 'Rocket Launcher', 'Cow Mangler 5000', "Liberty Launcher", "Direct Hit", "Rocket Jumper", "Beggar\'s Bazooka", "Air Strike", "Original"]
                sec = ["Shotgun", "Buff Banner", "Gunboats", "Battalion\'s Backup", "Concheror", "Mantreads", "Reserve Shooter", "Righteous Bison", "B.A.S.E. Jumper", "Panic Attack"]
                mle = ["Frying Pan", "Conscientious Objector", "Freedom Staff", "Bat Outta Hell", "Memory Maker", "Ham Shank", "Necro Smasher", "Crossing Guard", "Prinny Machete", "Saxxy", "Golden Frying Pan", "Shovel", "Equalizer", "Pain Train", "Half-Zatoichi", "Disciplinary Action", "Market Gardener", "Escape Plan"]
                pda = ["(not applicable)"]
            elif merc == "Pyro":
                pri = ["Flame Thrower", "Nostromo Napalmer", "Backburner", "Degreaser", "Phlogistinator", "Rainblower", "Dragon\'s Fury"]
                sec = ["Shotgun", "Panic Attack", "Reserve Shooter", "Thermal Thruster", "Flare Gun", "Detonator", "Scorch Shot", "Manmelter", "Gas Passer"]
                mle = ["Frying Pan", "Conscientious Objector", "Freedom Staff", "Bat Outta Hell", "Memory Maker", "Ham Shank", "Necro Smasher", "Crossing Guard", "Prinny Machete", "Saxxy", "Golden Frying Pan", "Fire Axe", "Axtinguisher", "Postal Pummeler", "Homewrecker", "Maul", "Powerjack", "Back Scratcher", "Sharpened Volcano Fragment", "Third Degree", "Lollichop", "Neon Annihilator", "Hot Hand"]
                pda = ["(not applicable)"]
            elif merc == "Demoman":
                mle = ["Frying Pan", "Conscientious Objector", "Freedom Staff", "Bat Outta Hell", "Memory Maker", "Ham Shank", "Necro Smasher", "Crossing Guard", "Prinny Machete", "Saxxy", "Golden Frying Pan", "Bottle", "Scottish Handshake", "Eyelander", "HHHH", "Nessie's Nine Iron", "Pain Train", "Scotsman's Skullcutter", "Claidheamh Mòr", "Ullapool Caber", "Half-Zatoichi", "Persian Persuader"]
                pri = ["Grenade Launcher", "Iron Bomber", "Loose Cannon", "Loch-n-Load", "Ali Baba\'s Wee Booties", "Bootlegger", "B.A.S.E. Jumper"]
                sec = ["Stickybomb Launcher", "Sticky Jumper", "Chargin\' Targe", "Splendid Screen", "Tide Turner", "Scottish Resistance", "Quickiebomb Launcher"]
                pda = ["(not applicable)"]
            elif merc == "Heavy":
                mle = ["Frying Pan", "Conscientious Objector", "Freedom Staff", "Bat Outta Hell", "Memory Maker", "Ham Shank", "Necro Smasher", "Crossing Guard", "Prinny Machete", "Saxxy", "Golden Frying Pan", "Fists", "Gloves of Runnings Urgently", "Killing Gloves of Boxing", "Eviction Notice", "Apoco-Fists", "Bread Bite", "Warrior\'s Spirit", "Fists of Steel", "Holiday Punch"]
                pri = ["Minigun", "Natascha", "Iron Curtain", "Tomislav", "Brass Beast", "Huo-Long Heater"]
                sec = ["Shogun", "Family Business", "Panic Attack", "Second Banana", "Dalokohs Bar", "Sanvich", "Robo-Sandvich", "Fishcake", "Buffalo Steak Sandvich"]
                pda = ["(not applicable)"]
            elif merc == "Engineer":
                mle = ["Golden Frying Pan", "Necro Smasher", "Saxxy", "Wrench", "Gunslinger", "Eureka Effect", "Jag", "Southern Hospitality", "Prinny Machete", "Golden Wrench"]
                pri = ["Shotgun", "Panic Attack", "Widowmaker", "Pomson 6000", "Rescue Ranger", "Frontier Justice"]
                sec = ["Giger Counter", "Wrangler", "Pistol", "Lugermorph", "C.A.P.P.E.R", "Short Circuit"]
                pda = ["Construction PDA"]
            elif merc == "Medic":
                mle = ["Frying Pan", "Conscientious Objector", "Freedom Staff", "Bat Outta Hell", "Memory Maker", "Ham Shank", "Necro Smasher", "Crossing Guard", "Prinny Machete", "Saxxy", "Golden Frying Pan", "Solemn Vow", "Ubersaw", "Vita-Saw", "Bonesaw", "Amputator"]
                pri = ["Syringe Gun", "Blutsauger", "Crusader\'s Crossbow", "Overdose"]
                sec = ["Kritzkrieg", "Quick-Fix", "Medi Gun", "Vaccinator"]
                pda = ["(not applicable)"]
            elif merc == "Sniper":
                mle = ["Frying Pan", "Conscientious Objector", "Freedom Staff", "Bat Outta Hell", "Memory Maker", "Ham Shank", "Necro Smasher", "Crossing Guard", "Prinny Machete", "Saxxy", "Golden Frying Pan", "Kukri", "Tribalman\'s Shiv", "Bushwacka", "Shahanshah"]
                pri = ["Sniper Rifle", "AWPer Hand", "Machina", "Classic", "Huntsman", "Fortified Compound", "Sydney Sleeper", "Shooting Star", "Bazaar Bargain", "Hitman\'s Heatmaker"]
                sec = ["SMG", "Jarate", "Self-Aware Beauty Mark", "Razorback", "Darwin\'s Danger Shield", "Cozy Camper", "Cleaner\'s Carbine"]
                pda = ["(not applicable)"]
            elif merc == "Spy":
                mle = ["Golden Frying Pan", "Saxxy", "Knife", "Prinny Machete", "Sharp Dresser", "Black Rose", "Your Eternal Reward", "Wanga Prick", "Big Earner", "Conniver's Kunai", "Spy-cicle"]
                pri = ["Ambassador", "Revolver", "Enforcer", "Diamondback", "L'Etranger", "Big Kill"]
                sec = ["Sapper", "Red-Tape Recorder", "Snack Attack", "Ap-Sap"]
                pda = ["Invis Watch", "Enthusiast\'s Timepiece", "Quäckenbirdt", "Cloak and Dagger", "Dead Ringer"]
        elif game == "Classic":
            if (merc := self.merc_cbox.currentText()) == "Scout":
                pri = ["Scattergun", "Nail Gun"]
                sec = ["Pistol", "Brick"]
                mle = ["Bat"]
                pda = ["(not applicable)"]
            elif merc == "Soldier":
                pri = ["Rocket Launcher", "RPG"]
                sec = ["Shotgun", "Gunboats"]
                mle = ["Shovel"]
                pda = ["(not applicable)"]
            elif merc == "Pyro":
                pri = ["Flame Thrower"]
                sec = ["Shotgun", "Flare Gun", "Twin Barrel"]
                mle = ["Fire Axe", "Harvester"]
                pda = ["(not applicable)"]
            elif merc == "Demoman":
                pri = ["Grenade Launcher", "Gunboats"]
                sec = ["Stickybomb Launcher", "Dynamite Pack"]
                mle = ["Bottle"]
                pda = ["(not applicable)"]
            elif merc == "Heavy":
                pri = ["Minigun", "Anti-Aircraft Cannon"]
                sec = ["Shotgun", "Sandvich"]
                mle = ["Fists", "Chekhov\'s Punch"]
                pda = ["(not applicable)"]
            elif merc == "Engineer":
                pri = ["Shotgun"]
                sec = ["Pistol", "Coilgun"]
                mle = ["Wrench"]
                pda = ["Construction PDA", "Jump Pad PDA"]
            elif merc == "Medic":
                pri = ["Syringe Gun"]
                sec = ["Medi Gun", "Kritzkreig"]
                mle = ["Bonesaw", "Shock Therapy","Überspritze"]
                pda = ["(not applicable)"]
            elif merc == "Sniper":
                pri = ["Sniper Rifle", "Huntsman", "Hunting Revolver"]
                sec = ["SMG"]
                mle = ["Kukri", "Fishwhacker"]
                pda = ["(not applicable)"]
            elif merc == "Spy":
                pri = ["Revolver", "Tranquilizer Gun"]
                sec = ["Sapper"]
                mle = ["Knife"]
                pda = ["Invis Watch", "L\'Escampette"]

        result = [(pri_, sec_, mle_, pda_) for pri_ in pri for sec_ in sec for mle_ in mle for pda_ in pda]
        llen = len(result)
        disallowed = ["Golden Wrench", "Golden Frying Pan", "Conscientious Objector", "Black Rose", "Wanga Prick", "Freedom Staff", "Frying Pan", "Saxxy", "Prinny Machete", "Bat Outta Hell", "Memory Maker", "Ham Shank", "Crossing Guard", "Necro Smasher", "Sharp Dresser", "Holy Mackerel", "Unarmed Combat", "Batsaber", "Scottish Handshake", "Nessie\'s Nine Iron", "Three-Rune Blade", "C.A.P.P.E.R", "Lugermorph", "Nostromo Napalmer", "Rainblower", "Lollichop", "Postal Pummeler", "Maul", "HHHH", "Mutated Milk"]
        if self.checkBox.isChecked():
            result = [(pri, sec, mle, pda) for (pri, sec, mle, pda) in result if pri not in disallowed and sec not in disallowed and mle not in disallowed and pda not in disallowed]
        
        if inargs("-debug"):
            print(result)
        if inargs("-saveongen"):
            with open("loadouts.txt", "w") as f:
                for loadout in result:
                    f.write(f"{loadout[0]}, {loadout[1]}, {loadout[2]}, {loadout[3]}\n")
            print("saved to loadout.txt")
        
        chosen_loadout = random.choice(result)
        self.label.setText(f"Game: {self.game_cbox.currentText()}\nMerc: {self.merc_cbox.currentText()}\nPrimary: {chosen_loadout[0]}\nSecondary: {chosen_loadout[1]}\nMelee: {chosen_loadout[2]}\nPDA: {chosen_loadout[3]}\n({llen - 1} more loadouts available)")
    def copy(self):
        clipboard.copy(f"""This is my automatically generated loadout, done with TF2 Loadout Maker 5000\n{self.label.text()}\nDownload TF2 Loadout Maker 5000 from https://gitlab.com/tf2-based/team-fortress-2-loadout-maker-5000""")
        msgbox = QMessageBox()
        msgbox.setText("Thanks for using TF2 Loadout Maker 5000!")
        msgbox.setWindowIcon(QtGui.QIcon("icon.ico"))
        msgbox.setIcon(QMessageBox.Icon.Information)
        msgbox.setWindowTitle("Thanks!")
        msgbox.exec_()

app = QApplication(sys.argv)
UIWindow = UI()
app.exec_()